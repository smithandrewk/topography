%% INIT
clear
addpath('data','utility')
%% LOAD
load('raw_data.mat');
% [time,acceleration] = import_from_aspire_csv("raw_aspire.csv");
%% RESAMPLE
[acceleration] = resample(acceleration,4,5);
%% BUFFER
buffered_acceleration = bufferRawByStep(acceleration,5,20);
%% CLASSIFY
classified_acceleration = classify_buffered_acceleration(buffered_acceleration);
%% VISUALIZE
animate_classification(classified_acceleration,buffered_acceleration,acceleration);
%% VISUALIZE STATE MACHINE
PUFFS = puff_dfa_visualize(buffered_acceleration,classified_acceleration);
%% STATE MACHINE TOPOGRAPHY
PUFFS = puff_dfa(buffered_acceleration,classified_acceleration);