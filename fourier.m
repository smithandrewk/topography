%% Initialization
clear
close all
clc
addpath('data')
addpath('utility')
%% Load and resample acceleration signal
load('raw_data.mat');
acc = resample(acceleration,4,5);
plot(acc)
legend('x','y','z');
title("Raw Accelerometer Data")
xlabel('Data Points (20 = 1 second)')
ylabel('Acc (m/s/s)')
%%
mag_acc = acc(:,1).^2+acc(:,2).^2+acc(:,3).^2;
kernel = mag_acc(1107:1131);
plot(mag_acc)
figure
plot(kernel)
%%
close
conv_mag = conv(mag_acc,kernel,"same")
plot(conv_mag)
%% X is less confusing
% I would like to extract the moment of hand on lip and hand off lip. How
% can I do this? What is the most salient aspect of that moment? Is it
% magnitude of acceleration? Is it gradient?
close
x = acc(:,1)
plot(x)
title('x accelerometer signal')
xlabel('Data Point (20 = 1 second')
ylabel('Acc (m/s/s)')
%% Convolution Theorem
close all
plot(x)

hold on
plot(convolution)
legend('x','conv')
% fftx = fft(x)
% padded = flip(padarray(kernels{3},length(x)-1,'post'))
% fftkernel = fft(padded)
% plot(ifft(fftx.*fftkernel))
%% Get Kernels
close
kernels = {}
kernels{1} = normalize(acc(190:225,1))
kernels{2} = flip(kernels{1})
kernels{3} = normalize(acc(1110:1190,1))
kernels{4} = [1,1,1,0,0,0];
for i=1:length(kernels)
   subplot(2,2,i)
   plot(kernels{i})
   title("Kernel: "+i)
end
%% CONV TOPOGRAPHY
convolution=normalize(conv(x,kernel,'same'));
x = normalize(x);
[~,hand_off_lip] = findpeaks(convolution,'MinPeakHeight',2,...
                                    'MinPeakDistance',100);
conv_inverted = -convolution;
[~,hand_on_lip] = findpeaks(conv_inverted,'MinPeakHeight',2,...
                                        'MinPeakDistance',100);

figure
hold on
plot(convolution)
plot(x)
plot(hand_off_lip,convolution(hand_off_lip),'rv','MarkerFaceColor','r')
plot(hand_on_lip,convolution(hand_on_lip),'rs','MarkerFaceColor','b')
for i=1:length(hand_off_lip)
   xline(hand_off_lip(i),'color','r') 
   xline(hand_on_lip(i),'color','g')
end
grid on
legend('Convolution','Hand-On-Lip','Hand-Off-Lip')
xlabel('Data Points (20 = 1 second)')
ylabel('Acc (m/s/s)')
title('Hand-To-Lip and Hand-Off-Lip from Accelerometer Data')
figure
plot(kernel)
%% Kernel 1
conv_topography(x,kernels{1})
