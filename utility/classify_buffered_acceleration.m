function [class] = classify_buffered_acceleration(buffered_acceleration)
%classify_buffered_acceleration Summary of this function goes here
%   Detailed explanation goes here
    %% CLASSIFY
    load SmokingNetInApp.mat new_net_v4;
    class = zeros(length(buffered_acceleration),1);
    for i =1:length(buffered_acceleration)
        if(new_net_v4(buffered_acceleration(i,:)')>.85)
            class(i,1) = 1;
        end
        if mod(i,100) == 0
            disp("Processed " + i + " out of " + length(buffered_acceleration))
        end
    end
end

