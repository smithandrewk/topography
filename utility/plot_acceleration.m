function plot_acceleration(time,acceleration)
%plot_acceleration plots an acceleration signal
%   Detailed explanation goes here
    plot(time,acceleration)
    title('Raw Acceleration From Watch');
    xlabel('Time (s)')
    ylabel('Acc (m/s/s)')
    legend('X','Y','Z');
end

