function acc_buff = bufferRawByStep(acc,windowSize, fs)
%to buffer accelerometer data into windows
% acc - accelerometer data
% windowSize - length of window in seconds
% fs - sampling frequency in Hz
samplingMoments = windowSize*fs;
cols = samplingMoments*3;
rows = (length(acc)-samplingMoments+1);
acc_buff = zeros(rows,cols);
new_acc = zeros(rows*cols,1);
i=1;
l = length(new_acc)
while(i<l)
    if(length(acc)<samplingMoments)
        break;
    end
    new_acc(i:i+(cols-1)) = [acc(1:samplingMoments,1);acc(1:samplingMoments,2);acc(1:samplingMoments,3)];
    i = i+cols;
    acc(1,:) = [];
end
for i = 1:rows
    if(length(new_acc)<cols)
        acc_buff(i,1:length(new_acc)) = new_acc;
        continue;
    end
    acc_buff(i,:) = new_acc(1:cols);
    new_acc(1:cols) = [];
    if mod(i,100) == 0
        clc
        disp("Processed " + i + " files out of " + rows)
    end
    
end

end