function output = lowpass_butter(signal,fs,filtCutOff)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
[b,  a] = butter(1, (2  *filtCutOff)/(fs), 'low');
output = filtfilt(b, a, signal);
end

