%% To visualize buffered puffs formatted for ANN in app
function puffs = puff_dfa(buffered_matrix,class)
    % Constants
    MINIMUM_PUFF_DURATION = .75; % seconds
    MINIMUM_REST_BETWEEN_PUFF = 2.5; % seconds
    MAXIMUM_REST_BETWEEN_PUFF = 240; % seconds
    MAXIMUM_SESSION_DURATION = 480; % seconds
    MINIMUM_NUMBER_OF_PUFFS = 3; % puffs
    SAMPLE_LENGTH = 1/20; % seconds
    
    % States
    HAND_ON_LIP = 'HANDONLIP';
    HAND_OFF_LIP = 'HANDOFFLIP';
    WAITING = "WAITING";
    STATE = WAITING;
    nl = "\newline";
    hl = "-------------------------------";
    %Variables to track parameters for topograpy
    CURRENT_REST_LENGTH = 0;
    CURRENT_PUFF_LENGTH = 0;
    puffs = [];
    curr_puff = Puff();
    %% Figure Loop
    for i=1:length(buffered_matrix)
        if(strcmp(STATE,WAITING))
            %STARTING PUFF
            if(class(i))% new puff
               STATE = HAND_ON_LIP;
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + SAMPLE_LENGTH;
               CURRENT_REST_LENGTH = 0;
               curr_puff = Puff();
               curr_puff.EndTime = i+100;
            else
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
            end
        elseif(strcmp(STATE,HAND_ON_LIP))
            %CONTINUING PUFF
            if(class(i))
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + SAMPLE_LENGTH;
            else
               curr_puff.StartTime = i;
               STATE = HAND_OFF_LIP;
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
            end
        elseif(strcmp(STATE,HAND_OFF_LIP))
            if(class(i))
               STATE = HAND_ON_LIP
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + CURRENT_REST_LENGTH + SAMPLE_LENGTH;
               CURRENT_REST_LENGTH = 0;
            else
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
               if(CURRENT_REST_LENGTH>MINIMUM_REST_BETWEEN_PUFF || ismembertol(CURRENT_REST_LENGTH,MINIMUM_REST_BETWEEN_PUFF,eps(CURRENT_REST_LENGTH)))
                  STATE = WAITING;
                  if(CURRENT_PUFF_LENGTH>MINIMUM_PUFF_DURATION || ismembertol(CURRENT_PUFF_LENGTH,MINIMUM_PUFF_DURATION,eps(CURRENT_PUFF_LENGTH)))
                  curr_puff.Length = curr_puff.EndTime-curr_puff.StartTime;
                  puffs = [puffs;curr_puff];
                  CURRENT_PUFF_LENGTH = 0;
                  curr_puff = Puff();
                  else
                     disp("PUFF WASNT LONG ENOUGH") 
                  end
               end
            end
        end
    end
end