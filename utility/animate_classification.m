function animate_classification(classification,buff,acc)
    %% VISUALIZE NETWORK
    a = linspace(1,100,100);
    b = linspace(101,200,100);
    c = linspace(201,300,100);
    h=figure;
    createline(h);
    classification = [zeros(99,1);classification];
    ylower = min(min(buff));
    yupper = max(max(buff));
    writerObj = VideoWriter('class_t.avi');% create the video writer with 1 fps
    writerObj.FrameRate = 10;% set the seconds per image
    open(writerObj);% open the video writer
    for i=1:length(classification)-299
        subplot(5,1,1);
        plot(a,buff(i,1:100),b,buff(i,101:200),c,buff(i,201:300));
        xticklabels({'0','50','100/0','50','100/0','50','100'});
        ylim([ylower-3,yupper+7]);
        title('Current ANN Input');
        subplot(4,1,[2,3,4])
        plot(linspace(1,300,300),acc(i:i+299,:),linspace(1,300,300),classification(i:i+299,:)*5+21);
        xticklabels({'0','50','Present','150','200','250','300'});

        legend('x','y','z','ANN OUTPUT')
        ylim([ylower-3,yupper+7]);
        xlabel('Data Points ( 20 = 1 second )')
        if(classification(i+99)==1)
            title('PUFF')
            set(gca,'color',[0 1 0 .3])
            pause(.05)
        else
            title('NOT PUFF')
        end


        writeVideo(writerObj,getframe(gcf))% write the frames to the video
        
    end
    close(writerObj);% close the writer object
end