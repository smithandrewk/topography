function visualize_buffered_acceleration(buffered_matrix)
    %% Init stack
    h=gcf;
    set(gcf, 'KeyPressFcn', @myKeyPressFcn,'Position',[50,50,1300,700]);
    global KEY_IS_PRESSED
    global I
    global END
    global ACCEL
    I = 1;
    KEY_IS_PRESSED = 0;
    END = 0;
    ACCEL = 0;
    %% Figure Loop
    while(I < length(buffered_matrix))
        plot(linspace(1,100,100),buffered_matrix(I,1:100),linspace(101,200,100),buffered_matrix(I,101:200),linspace(201,300,100),buffered_matrix(I,201:300));
        str = "Buffered Acceleration Starting From "+((I-1)/20)+" Seconds";
        title(str);
        xlabel('Time (s)')
        xticklabels({'0','50','100/0','50','100/0','50','100'});
        ylabel('Acc (m/s/s)')
        legend('X','Y','Z');
        while ~KEY_IS_PRESSED
            pause(.01);
            if(~ishandle(h))
                END = 1;
                break;
            end
        end
        KEY_IS_PRESSED = 0;
        if(END)
            close;
            clc;
            break;
        end
    end
    close;
end
%% on key press, call this function
function myKeyPressFcn(hObject, event)
    global I
    global END
    global KEY_IS_PRESSED
    global ACCEL
    KEY_IS_PRESSED  = 1;
    %ACCELERATED MODE (increments by 5)
    if(ACCEL)
        if(strcmp(event.Key,"rightarrow"))
            I=I+5;
        elseif(strcmp(event.Key,"leftarrow")&&I<=5)
            END = 1;
        elseif(strcmp(event.Key,"leftarrow"))
            I=I-5;
        elseif(strcmp(event.Key,"control"))
            ACCEL=0;
        else
            END=1;
        end
    %NON-ACCELERATED MODE (increments by 1)    
    else
        if(strcmp(event.Key,"rightarrow"))
            I=I+1;
        elseif(strcmp(event.Key,"leftarrow")&&I==1)
            END = 1;
        elseif(strcmp(event.Key,"leftarrow"))
            I=I-1;
        elseif(strcmp(event.Key,"control"))
            ACCEL=1;
        else
            END=1;
        end
    end
end