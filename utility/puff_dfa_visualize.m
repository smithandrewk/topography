%% To visualize buffered puffs formatted for ANN in app
function ps = puff_dfa_visualize(buffered_matrix,class)
    % Constants
    MINIMUM_PUFF_DURATION = .75; % seconds
    MINIMUM_REST_BETWEEN_PUFF = 2.5; % seconds
    MAXIMUM_REST_BETWEEN_PUFF = 240; % seconds
    MAXIMUM_SESSION_DURATION = 480; % seconds
    MINIMUM_NUMBER_OF_PUFFS = 3; % puffs
    WINDOW_SIZE = 5; % seconds
    SAMPLE_LENGTH = 1/20; % seconds
    
    % States
    HAND_ON_LIP = 'HANDONLIP';
    HAND_OFF_LIP = 'HANDOFFLIP';
    WAITING = "WAITING";
    STATE = WAITING;
    nl = "\newline";
    hl = "-------------------------------";
    %Variables to track parameters for topograpy
    CURRENT_REST_LENGTH = 5;
    CURRENT_PUFF_LENGTH = 0;
    PREVIOUS_REST_LENGTH = 0;
    PUFFS = [];
    ps = [];
    %% Init stack
    h= subplot(1,3,1:2);
    set(gcf, 'KeyPressFcn', @myKeyPressFcn,'Position',[100 100 1200 500]);
    global KEY_IS_PRESSED
    global I
    global END
    global ACCEL
    I = 1;
    KEY_IS_PRESSED = 0;
    END = 0;
    ACCEL = 0;
    curr_puff = Puff();
    %% Figure Loop
    while(I < length(buffered_matrix))
        plot(linspace(1,100,100),buffered_matrix(I,1:100),linspace(101,200,100),buffered_matrix(I,101:200),linspace(201,300,100),buffered_matrix(I,201:300));
        if(class(I))
            set(gca,'color',[0 1 0])
        end

        if(strcmp(STATE,WAITING))
            %STARTING PUFF
            if(class(I))% new puff
               STATE = HAND_ON_LIP;
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + SAMPLE_LENGTH;
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH-5; %lower bound
               curr_puff = Puff();
               curr_puff.EndTime = I+100;
            else
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
            end
        elseif(strcmp(STATE,HAND_ON_LIP))
            %CONTINUING PUFF
            if(class(I))
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + SAMPLE_LENGTH;
            else
               curr_puff.StartTime = I;
               STATE = HAND_OFF_LIP;
               CURRENT_REST_LENGTH = CURRENT_PUFF_LENGTH+SAMPLE_LENGTH;
            end
        elseif(strcmp(STATE,HAND_OFF_LIP))
            if(class(I))
               STATE = HAND_ON_LIP
               CURRENT_PUFF_LENGTH = CURRENT_PUFF_LENGTH + CURRENT_REST_LENGTH + SAMPLE_LENGTH;
               CURRENT_REST_LENGTH = 0;
            else
               CURRENT_REST_LENGTH = CURRENT_REST_LENGTH + SAMPLE_LENGTH;
               if(CURRENT_REST_LENGTH>MINIMUM_REST_BETWEEN_PUFF || ismembertol(CURRENT_REST_LENGTH,MINIMUM_REST_BETWEEN_PUFF,eps(CURRENT_REST_LENGTH)))
                  STATE = WAITING;
                  if(CURRENT_PUFF_LENGTH>MINIMUM_PUFF_DURATION || ismembertol(CURRENT_PUFF_LENGTH,MINIMUM_PUFF_DURATION,eps(CURRENT_PUFF_LENGTH)))
                  curr_puff.Length = curr_puff.EndTime-curr_puff.StartTime;
                  ps = [ps;curr_puff];
                  PUFFS = [PUFFS,I];
                  CURRENT_PUFF_LENGTH = 0;
                  curr_puff = Puff();
                  else
                     disp("PUFF WASNT LONG ENOUGH") 
                  end
               end
            end
        end
        str = {hl+nl+STATE+nl+"i="+"\color{red}"+I+"\color{black}"+nl
               "Current Puff Upper Bound: "+"\color{red}"+(WINDOW_SIZE-CURRENT_PUFF_LENGTH)+"\color{black}"+nl
               "Current Rest Length:"+"\color{red}"+CURRENT_REST_LENGTH+"\color{black}"+nl
               "Number Of Puffs: "+"\color{red}"+length(PUFFS)+"\color{black}"+nl
               hl};
        text(500,150,str,'Units','points','FontSize',20)

        while ~KEY_IS_PRESSED
            pause(.01);
            if(~ishandle(h))
                END = 1;
                break;
            end
        end
        KEY_IS_PRESSED = 0;
        if(END)
            close;
            clc;
            break;
        end
    end
    close;
end

%% on key press, call this function
function myKeyPressFcn(hObject, event)
    global I
    global END
    global KEY_IS_PRESSED
    global ACCEL
    KEY_IS_PRESSED  = 1;
    
%     disp("Key is pressed");
    %ACCELERATED MODE (increments by 5)
    if(ACCEL)
        if(strcmp(event.Key,"rightarrow"))
            I=I+5;
        elseif(strcmp(event.Key,"leftarrow")&&I<=5)
            END = 1;
        elseif(strcmp(event.Key,"leftarrow"))
            I=I-5;
        elseif(strcmp(event.Key,"control"))
            ACCEL=0;
        else
            END=1;
        end
    %NON-ACCELERATED MODE (increments by 1)    
    else
        if(strcmp(event.Key,"rightarrow"))
            I=I+1;
        elseif(strcmp(event.Key,"leftarrow")&&I==1)
            END = 1;
        elseif(strcmp(event.Key,"leftarrow"))
            I=I-1;
        elseif(strcmp(event.Key,"control"))
            ACCEL=1;
        else
            END=1;
        end
    end
end