function plot_buffered_acceleration(i,buffered_acceleration)
    plot(linspace(1,100),buffered_acceleration(i,1:100),linspace(101,200),buffered_acceleration(i,101:200),linspace(201,300),buffered_acceleration(i,201:300))
%     str = "Buffered Acceleration Starting From "+((i-1)/20)+" Seconds";
%     title(str);
%     xlabel('Time (s)')
%     xticklabels({'0','50','100/0','50','100/0','50','100'});
%     ylabel('Acc (m/s/s)')
%     legend('X','Y','Z');
end