function conv_topography(x,kernel)
    convolution=normalize(conv(x,kernel,'same'));
    x = normalize(x);
    [~,hand_off_lip] = findpeaks(convolution,'MinPeakHeight',2,...
                                        'MinPeakDistance',100);
    conv_inverted = -convolution;
    [~,hand_on_lip] = findpeaks(conv_inverted,'MinPeakHeight',2,...
                                            'MinPeakDistance',100);
                                        
    figure
    hold on
    plot(convolution)
    plot(x)
    plot(hand_off_lip,convolution(hand_off_lip),'rv','MarkerFaceColor','r')
    plot(hand_on_lip,convolution(hand_on_lip),'rs','MarkerFaceColor','b')
    for i=1:length(hand_off_lip)
       xline(hand_off_lip(i),'color','r') 
       xline(hand_on_lip(i),'color','g')
    end
    grid on
    legend('Convolution','Hand-On-Lip','Hand-Off-Lip')
    xlabel('Data Points (20 = 1 second)')
    ylabel('Acc (m/s/s)')
    title('Hand-To-Lip and Hand-Off-Lip from Accelerometer Data')
    figure
    plot(kernel)
end