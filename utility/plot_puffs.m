function plot_puffs(acceleration,puffs)
    plot(acceleration)
    % axis colors
    matgreen =[0.203, 0.603, 0.156];
    matred = [0.933, 0.266, 0.090];
    matblue = [0.00,0.45,0.74]; % x axis color
    mator = [0.85,0.33,0.10]; % y axis color
    matyel = [0.93,0.69,0.13]; % z axis color
    for i=1:length(puffs)
        xline(puffs(i).StartTime,'Color',matgreen,'LineWidth',2)
        xline(puffs(i).EndTime,'Color',matred,'LineWidth',2)
    end
    title("Puff DFA Analysis")
    legend('x','y','z','puff start', 'puff end')
    xlabel("Data point (20 = 1 second)")
    ylabel("m/s/s")
end